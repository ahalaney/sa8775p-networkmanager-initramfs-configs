#! /usr/bin/bash

# Prerequisite check(s) for module
check() {
	# Install by default
	return 0
}

# echo module dependency requirements
depends() {
	# These profiles make no sense without network-manager module
	echo "network-manager"

	# According to Eric, but the man page has no mention of this
	# Return 0 to include the dependent module(s) in the initramfs.
	return 0
}

# Install the required file(s) and directories for the module in the initramfs.
install() {
	inst_multiple -o \
		/etc/NetworkManager/system-connections/end0-static.connection \
		/etc/NetworkManager/system-connections/end1-static.connection
}
