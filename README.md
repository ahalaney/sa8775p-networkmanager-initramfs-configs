# sa8775p-networkmanager-initramfs-configs

This project illustrates how one can package
NetworkManager profiles, and install them
in an initramfs for usage.


Specifically this maps to the sa8775p-ride's
two interfaces when using the devicetree based
persistent naming (end0, end1). It sets
two static IPs on the devices. There's a
dracut module to install it in the initramfs
as well.


Building should be easy, using:
```
rpkg local
```
should result in an RPM you can install on target. On qualcomm platforms be sure
to regenerate a boot image!


Alternatively, installation at image creation time can be done. Something like:
```
createrepo_c /path/to/rpm/dir`
make DEFINES='extra_repos=[{"id":"local","baseurl":"file:///path/to/rpm/dir"}] extra_rpms=["sa8775p-networkmanager-initramfs-configs"]' cs9-ridesx4-developer-regular.aarch64.aboot
```
should get you going.


Using a modified sample-images over here: https://gitlab.com/ahalaney/sample-images/-/tree/stmmac-initramfs
shows how you can get the ethernet devices to come up in the initramfs
(what modules are needed, command line arguments, etc).
